package com.cheetah.seckill.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cheetah.seckill.mapper.SeckillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seckill")
public class SeckillController {
    @Autowired
    private SeckillMapper seckillMapper;
    @Value("${project.path}")
    private String projectPath;

    @RequestMapping("/list")
    public Object list() {
        return this.seckillMapper.selectList(new QueryWrapper<>());
    }
}
