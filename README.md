
# 开发环境

JDK1.8、Maven、Mysql、IntelliJ IDEA、redis3.0以上、rocketmq

# 启动说明

- 启动前 请配置 application.properties 中相关mysql、redis以及rocketmq相关地址，建议在Linux下安装使用。

- 数据库脚本在项目根目录下的seckill.sql，启动前请自行导入。

- 配置完成，运行com.cheetah.seckill.App中的main方法

- 本案例单纯为了学习。

# 常用接口
- 压力测试，200个线程抢100个库存
http://127.0.0.1:8080/test/seckill/start?id=1000
- 正常的秒杀
http://127.0.0.1:8080/seckill/start?id=1000
- 创建商品静态页面
http://127.0.0.1:8080/seckill/createHtml?id=1000

# 安装rocketmq

建议在Linux环境安装，需要依赖jdk，建议jdk1.8。

我用的是rocketmq-all-4.4.0.zip，先把它上传到Linux服务器，之后解压。

比如我们解压到**/opt/soft/rocketmq-all-4.4.0-bin-release**

## 修改环境变量

**vi /etc/profile**

```
export JAVA_HOME=/opt/soft/jdk1.8.0_45
export PATH=$PATH:$JAVA_HOME/bin:/opt/soft/apache-hive-2.3.6-bin/bin:/opt/soft/hadoop-2.9.2/bin
export ROCKETMQ_HOME=/opt/soft/rocketmq-all-4.4.0-bin-release
```

修改以上的代码，之后执行**source /etc/profile**

## 修改rocketmq相关脚本

cd /opt/soft/rocketmq-all-4.4.0-bin-release

vi bin/runserver.sh

```
JAVA_OPT="${JAVA_OPT} -server -Xms768m -Xmx768m -Xmn768m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```

修改以上内容，不用分配那么大内存。

vi bin/runbroker.sh

```
JAVA_OPT="${JAVA_OPT} -server -Xms768m -Xmx768m -Xmn768m"
```

vi conf/broker.conf (看情况修改，有时候broker绑定的ip并不对)

```
brokerIP1=192.168.3.24
brokerClusterName = DefaultCluster
brokerName = broker-a
brokerId = 0
deleteWhen = 04
fileReservedTime = 48
brokerRole = ASYNC_MASTER
flushDiskType = ASYNC_FLUSH
```

之后启动

 sh bin/mqnamesrv &

 sh bin/mqbroker -n 127.0.0.1:9876 -c conf/broker.conf &

## 验证

jps

```
6101 Jps
6011 NamesrvStartup
6043 BrokerStartup
```

