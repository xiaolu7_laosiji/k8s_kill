/*
 Navicat Premium Data Transfer

 Source Server         : 172.20.128.113
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : 172.20.128.113:3306
 Source Schema         : cheetah

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 03/09/2020 15:20:28
*/

create database cheetah;
use cheetah;
SET NAMES utf8mb4;

-- ----------------------------
-- Table structure for seckill
-- ----------------------------
DROP TABLE IF EXISTS `seckill`;
CREATE TABLE `seckill`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `version` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1001 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of seckill
-- ----------------------------
INSERT INTO `seckill` VALUES (1000, '拯救者R7000', 100, '2020-08-01 15:26:02', '2020-09-30 15:26:08', '2020-09-02 15:25:53', 0);
CREATE TABLE `t_order` (
  `id` bigint(20) NOT NULL,
  `seckill_id` int(11) DEFAULT NULL,
  `state` tinyint(3) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
